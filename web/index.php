<?php

use Butenko\Controllers\PriceController;
use Butenko\Services\Parser;
use Butenko\Services\Finder;

require_once('../vendor/autoload.php');

switch ($_GET['page']) {
    case 'proposes':
        $controller = new PriceController();
        echo($controller->resultAction(new Parser(), new Finder()));
    break;
    default:
        $controller = new PriceController();
        echo($controller->homeAction());
    break;
}

