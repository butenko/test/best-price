<?php

namespace Tests;

/**
 * Class ReflectionTestTrait.
 */
trait ReflectionTestTrait
{
    /** @var \ReflectionClass */
    protected $reflection;

    /**
     * Invoke method.
     *
     * @param object $object
     * @param string $method
     * @param array  $args
     *
     * @return mixed
     */
    protected function invokeMethod($object, $method, $args = [])
    {
        return $this->createAccessibleMethod($object, $method)->invokeArgs($object, $args);
    }

    /**
     * Create accessible method.
     *
     * @param object $object
     * @param string $method
     *
     * @return \ReflectionMethod
     */
    protected function createAccessibleMethod($object, $method)
    {
        $method = new \ReflectionMethod($object, $method);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * @param object $object
     * @param string $name
     * @param mixed  $value
     *
     * @return \ReflectionProperty
     */
    protected function injectProperty($object, $name, $value)
    {
        $property = new \ReflectionProperty($object, $name);
        $property->setAccessible(true);
        $property->setValue($object, $value);

        return $property;
    }

    /**
     * Fetch property from object.
     *
     * @param object $object
     * @param string $name
     *
     * @return mixed
     */
    protected function fetchProperty($object, $name)
    {
        $property = new \ReflectionProperty($object, $name);
        $property->setAccessible(true);

        return $property->getValue($object);
    }
}
