<?php

namespace Tests\Butenko\Services;

use Butenko\Models\ComponentModel;
use Butenko\Models\ItemModel;
use Butenko\Models\LocationModel;
use Butenko\Services\Finder;
use Tests\AbstractBaseTest;
use Tests\ReflectionTestTrait;

class FinderTest extends AbstractBaseTest
{
    use ReflectionTestTrait;

    /** @covers Finder::findBestPropose() */
    public function testFindBestPropose()
    {
        $f_components = ['hot_dog', 'hamburger'];
        $locations    = [];
        $mock         = \Mockery::mock(Finder::class)->makePartial()->shouldAllowMockingProtectedMethods();
        $mock
            ->shouldReceive('analiseProposes')->with($locations, $f_components)
            ->shouldReceive('compareComponents')->with($f_components);

        $this->injectProperty($mock, 'price_list', [2 => 'bed_price', 1 => 'best_price']);

        $result = $mock->findBestPropose($locations, $f_components);
        $this->assertEquals($result, 'best_price');
    }

    /**
     * @covers Finder::analiseProposes()
     * @covers Finder::updatePriceList()
     */
    public function testAnaliseProposesPriseList()
    {
        $f_components = ['hot_dog', 'hamburger'];
        $item         = new ItemModel(15, [new ComponentModel('hot_dog')]);

        $location = new LocationModel($location_id = 1, $item);
        $location
            ->addMenuItem(new ItemModel(5, [new ComponentModel('hamburger')]))
            ->addMenuItem(new ItemModel(19, [new ComponentModel('hamburger'), new ComponentModel('hot_dog')]));

        $class = new Finder();
        $this->invokeMethod($class, 'analiseProposes', [[$location], $f_components]);

        $partitions = $this->fetchProperty($class, 'partitions');
        $price_list = $this->fetchProperty($class, 'price_list');

        $partitions_item = [
            $location_id => [
                'hamburger' => 5,
                'hot_dog'   => 15,
            ],
        ];

        $this->assertEquals($partitions, $partitions_item);

        $price_list_item = [
            'location_id' => 1,
            'extra_count' => 0,
            'label'       => implode(', ', $f_components),
            'price'       => 19,
        ];
        $this->assertEquals($price_list[19], $price_list_item);
    }

    /**
     * @covers Finder::compareComponents()
     */
    public function testCompareComponents()
    {
        $class      = new Finder();
        $partitions = [
            1 => [
                'hamburger' => 5,
                'hot_dog'   => 15,
            ],
        ];
        $this->injectProperty($class, 'partitions', $partitions);
        $this->invokeMethod($class, 'compareComponents', [['hot_dog', 'hamburger']]);

        $price_list      = $this->fetchProperty($class, 'price_list');
        $price_list_item = [
            'location_id' => 1,
            'extra_count' => -1,
            'label'       => 'hamburger, hot_dog',
            'price'       => 20,
        ];
        $this->assertEquals($price_list[20], $price_list_item);
    }
}
