<?php

namespace Tests\Butenko\Services;

use Butenko\Services\Parser;
use Tests\AbstractBaseTest;
use Tests\ReflectionTestTrait;

/**
 * Class ParserTest
 *
 * @package Tests\Butenko\Services
 */
class ParserTest extends AbstractBaseTest
{
    use ReflectionTestTrait;

    /** @var Parser class */
    protected $class;


    public function setUp()
    {
        parent::setUp();

        $this->class = new Parser();
    }

    /** @covers Parser::parseUploadedCSV() */
    public function testParseUploadedCSV()
    {
        file_put_contents('data.csv', '1, 4.00, hot_dog');
        $_FILES['csv'] = [
            'tmp_name' => 'data.csv',
        ];

        $locations = $this->class->parseUploadedCSV();
        $id        = $locations[1]->getId();
        $price     = ((($locations[1])->getMenuItems())[0])->getPrice();
        $component = ((((($locations[1])->getMenuItems())[0])->getComponents())[0])->getName();
        $this->assertEquals($id, 1);
        $this->assertEquals($price, 4);
        $this->assertEquals($component, 'hot_dog');
        unlink('data.csv');
    }


    /**
     * @param array $csv_elements
     * @param       $message
     *
     * @covers       Parser::validateCSV()
     * @dataProvider validateCsvDataProvider()
     */
    public function testValidateCSV(array $csv_elements, $message)
    {
        if ($message) {
            $this->expectExceptionMessage($message);
            $this->invokeMethod($this->class, 'validateCSV', [$csv_elements]);
        } else {
            $result = $this->invokeMethod($this->class, 'validateCSV', [$csv_elements]);
            $this->assertEquals(true, $result);
        }
    }

    public function validateCsvDataProvider()
    {
        return [
            [[1, 27.5, 'hot_dog'], $message = false],
            [['1', '345.1', 'hot_dog'], $message = false],
            [['1', 'dsa', 'hot_dog'], 'Invalid price in csv. Only float pls'],
            [['1', '-345.1', 'hot_dog'], 'Invalid price in csv. Price must be > 0'],
            [['1', '-345.1'], 'Invalid csv'],
            [['avs', '345.1', 'hot_dog'], 'Invalid location_id in csv. Only int pls'],
            [['hot_dog', '-345.1', '1'], 'Invalid location_id in csv. Only int pls'],
        ];
    }

    /**
     * @param $component
     * @param $message
     *
     * @covers       Parser::validateComponent()
     * @dataProvider validateComponentDataProvider()
     */
    public function testValidateComponent($component, $message)
    {
        if ($message) {
            $this->expectExceptionMessage($message);
            $this->invokeMethod($this->class, 'validateComponent', [$component]);
        } else {
            $result = $this->invokeMethod($this->class, 'validateComponent', [$component]);
            $this->assertEquals(true, $result);
        }

    }

    public function validateComponentDataProvider()
    {
        return [
            ['hamburger', $message = false],
            ['hot_dog', $message = false],
            [
                'hot_dog, hamburger',
                'Invalid component name, only en string (a-z) in lower case letters and underscores',
            ],
            ['hot dog', 'Invalid component name, only en string (a-z) in lower case letters and underscores'],
            ['hotDog', 'Invalid component name, only en string (a-z) in lower case letters and underscores'],
            ['hotd0g', 'Invalid component name, only en string (a-z) in lower case letters and underscores'],
            ['hotdog$', 'Invalid component name, only en string (a-z) in lower case letters and underscores'],
            ['', 'Component with empty name! Error!'],
        ];
    }

    /** @covers Parser::parseComponents() */
    public function testParseComponents()
    {
        $components_string = 'hot_dog, hamburger';
        $result            = $this->class->parseComponents($components_string);
        $this->assertEquals(explode(', ', $components_string), $result);
    }
}
