<?php

namespace Butenko\Services;

use Butenko\Models\LocationModel;
use Butenko\Models\ItemModel;

/**
 * Class Finder
 *
 * @package Butenko\Services
 */
interface FinderInterface
{
    /**
     * @param array $locations
     * @param array $f_components
     */
    public function findBestPropose(array $locations, array $f_components);
}