<?php

namespace Butenko\Services;

/**
 * Interface ParserInterface
 *
 * @package Butenko\Services
 */
interface ParserInterface
{
    public function parseUploadedCSV();

    /**
     * @param $components
     *
     * @return mixed
     */
    public function parseComponents($components);
}