<?php

namespace Butenko\Services;

use Butenko\Models\LocationModel;
use Butenko\Models\ItemModel;

/**
 * Class Finder
 *
 * @package Butenko\Services
 */
class Finder implements FinderInterface
{
    protected $partitions = [];

    protected $price_list = [];

    /**
     * @param array $locations
     * @param array $f_components
     */
    public function findBestPropose(array $locations, array $f_components)
    {
        $this->analiseProposes($locations, $f_components);
        $this->compareComponents($f_components);
        ksort($this->price_list);

        return array_values($this->price_list)[0];
    }

    /**
     * @param array $locations
     * @param array $f_components
     */
    protected function analiseProposes(array $locations, array $f_components)
    {
        /** @var LocationModel $location */
        foreach ($locations as $location) {
            /** @var ItemModel $menu_item */
            foreach ($location->getMenuItems() as $menu_item) {
                $a_components       = array_intersect($f_components, $menu_item->getComponents());
                $a_components_count = count($a_components);
                $f_components_count = count($f_components);
                $menu_item_count    = count($menu_item->getComponents());
                $extra_count        = 0;
                $price              = $menu_item->getPrice();

                //if no components
                if ($a_components_count <= 0) {
                    continue;
                }
                //if menu has extra components
                if ($menu_item_count != $a_components_count) {
                    $extra_count = $menu_item_count - $a_components_count;
                }

                //full components
                if ($a_components_count == $f_components_count) {
                    $this->updatePriceList($location->getId(), $price, implode(', ', $a_components), $extra_count);
                    //single components
                } elseif ($extra_count == 0) {
                    if (array_key_exists($location->getId(), $this->partitions)) {
                        if (array_key_exists(implode(', ', $a_components), $this->partitions[ $location->getId() ])) {
                            if ($price >= $this->partitions[ $location->getId() ][ implode(', ', $a_components) ]) {
                                continue;
                            }
                        }
                        $this->partitions[ $location->getId() ][ implode(', ', $a_components) ] = $price;
                    } else {
                        $this->partitions[ $location->getId() ] = [implode(', ', $a_components) => $price];
                    }
                }
            }
        }
    }

    /**
     * @param $f_components
     */
    protected function compareComponents($f_components)
    {
        foreach ($this->partitions as $locale_id => $partitions) {
            $sum = 0;
            foreach ($partitions as $cost) {
                $sum += $cost;
            }
            if (count($f_components) == count($partitions)) {
                $this->updatePriceList($locale_id, $sum, implode(', ', array_keys($partitions)));
            }
        }
    }

    /**
     * @param     $location_id
     * @param     $price
     * @param     $label
     * @param int $extra_count
     *
     * @return bool
     */
    protected function updatePriceList($location_id, $price, $label, $extra_count = -1)
    {
        if (array_key_exists($price, $this->price_list)) {
            if ($extra_count >= $this->price_list[ $price ]['extra_count']) {
                return false;
            }
        }
        $this->price_list[ $price ] = [
            'location_id' => $location_id,
            'extra_count' => $extra_count,
            'label'       => $label,
            'price'       => $price,
        ];
        return true;
    }
}