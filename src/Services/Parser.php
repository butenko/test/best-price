<?php

namespace Butenko\Services;

use Butenko\Models\ComponentModel;
use Butenko\Models\ItemModel;
use Butenko\Models\LocationModel;

/**
 * Class Parser
 *
 * @package Butenko\Services
 */
class Parser implements ParserInterface
{
    /**
     * @return array
     * @throws \Exception
     */
    public function parseUploadedCSV()
    {
        if($csv_array = file($_FILES['csv']['tmp_name']))
        {
            $locations = [];
            foreach ($csv_array as $csv_string) {
                $parsed_data = str_getcsv(str_replace(' ', '', $csv_string), ',');
                $this->validateCSV($parsed_data);
                $components = [];
                foreach (array_slice($parsed_data, 2) as $component) {
                    $this->validateComponent($component);
                    $components[] = new ComponentModel($component);
                }
                $menu_item = new ItemModel($parsed_data[1], $components);

                if (array_key_exists($parsed_data[0], $locations)) {
                    $locations[ $parsed_data[0] ]->addMenuItem($menu_item);
                } else {
                    $locations[ $parsed_data[0] ] = new LocationModel($parsed_data[0], $menu_item);
                }
            }
            return $locations;
        }
        throw new \Exception('Invalid csv');
    }

    /**
     * @param array $parsed_data
     *
     * @return bool
     * @throws \Exception
     */
    protected function validateCSV(array $parsed_data)
    {
        if (count($parsed_data) < 3) {
            throw new \Exception('Invalid csv');
        }

        if (!is_numeric($parsed_data[0])) {
            throw new \Exception('Invalid location_id in csv. Only int pls');
        }

        if (!is_numeric($parsed_data[1])) {
            throw new \Exception('Invalid price in csv. Only float pls');
        }

        if ($parsed_data[1] < 0) {
            throw new \Exception('Invalid price in csv. Price must be > 0');
        }

        return true;

    }

    /**
     * @param $component
     *
     * @return bool
     * @throws \Exception
     */
    protected function validateComponent($component)
    {
        $reg_exp = '/[a-z_]*/';
        preg_match($reg_exp, $component, $reg_exp_rez);
        if($component == ''){
            throw new \Exception('Component with empty name! Error!');
        }
        if ($component != $reg_exp_rez[0]) {
            throw new \Exception('Invalid component name, only en string (a-z) in lower case letters and underscores');
        }
        return true;
    }

    /**
     * @param string $components
     *
     * @return array
     */
    public function parseComponents($components)
    {
        return explode(', ', $components);
    }
}