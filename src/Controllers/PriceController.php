<?php

namespace Butenko\Controllers;

use Butenko\Services\FinderInterface;
use Butenko\Services\ParserInterface;

/**
 * Class PriceController
 *
 * @package Butenko\Controllers
 */
class PriceController
{
    /**
     * @param ParserInterface $parser
     * @param FinderInterface $finder
     *
     * @return string
     */
    public function resultAction(ParserInterface $parser, FinderInterface $finder)
    {
        if (empty($_POST['components'])) {
            return 'No result, pls enter components';
        }
        try {
            $locations = $parser->parseUploadedCSV();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
        $components   = $parser->parseComponents($_POST['components']);
        $best_propose = $finder->findBestPropose($locations, $components);
        require_once('Templates/ResultTemplate.php');
    }

    public function homeAction()
    {
        require_once('Templates/HomeTemplate.php');
    }
}