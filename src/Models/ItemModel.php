<?php

namespace Butenko\Models;

/**
 * Class ItemModel
 *
 * @package Butenko\Models
 */
class ItemModel
{
    /** @var  double */
    protected $price;

    /** @var array */
    protected $components = [];

    /**
     * ItemModel constructor.
     *
     * @param null       $price
     * @param array|null $components
     */
    public function __construct($price = null, array $components = null)
    {
        $this->price      = $price;
        $this->components = $components;
    }

    /**
     * @param $component
     *
     * @return $this
     */
    public function addComponent(ComponentModel $component)
    {
        $this->components[] = $component;

        return $this;
    }

    /**
     * @return array
     */
    public function getComponents()
    {
        return $this->components;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
}