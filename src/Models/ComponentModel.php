<?php

namespace Butenko\Models;

/**
 * Class ComponentModel
 *
 * @package Butenko\Models
 */
class ComponentModel
{
    /** @var  string */
    protected $name;

    /**
     * ComponentModel constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}