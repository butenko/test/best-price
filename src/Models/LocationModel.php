<?php

namespace Butenko\Models;

/**
 * Class LocationModel
 *
 * @package Butenko\Models
 */
class LocationModel
{
    /** @var  int */
    protected $id;

    /** @var array */
    protected $menu = [];

    /**
     * LocationModel constructor.
     *
     * @param           $id
     * @param ItemModel $menu_item
     */
    public function __construct($id = null, ItemModel $menu_item = null)
    {
        $this->id     = $id;
        $this->menu[] = $menu_item;
    }

    /**
     * @param ItemModel $menu_item
     *
     * @return $this
     */
    public function addMenuItem(ItemModel $menu_item)
    {
        $this->menu[] = $menu_item;

        return $this;
    }

    public function getMenuItems()
    {
        return $this->menu;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}